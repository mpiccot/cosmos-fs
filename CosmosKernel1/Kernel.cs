﻿using System;
using System.Collections.Generic;
using System.Text;
using Sys = Cosmos.System;
using System.Threading;
using Cosmos.Hardware.BlockDevice;
using System.IO;
using FAT = Cosmos.System.Filesystem.FAT;
using CosmosKernel1.Helpers;
using CosmosKernel1.FileSystem;

namespace CosmosKernel1
{
    public class Kernel : Sys.Kernel
    {      
        protected override void BeforeRun()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.BackgroundColor = ConsoleColor.Black;
            try
            {
                if (Initializer.Initialize() != GLOBALS.ReturnCode.Success)
                {
                    this.Stop();
                    return;
                }
            }
            catch (Exception ex)
            {
                Helper.eWriteLine("Error!" + ex.Message);
                this.Stop();
                return;
            }

            FileTable.DeserializeAndLoad();
            Helper.iWriteLine(GLOBALS.OSNameASCIILogo);
            Helper.iWriteLine("Type 'help' for a list of commands");
        }

        protected override void Run()
        {
            Shell.beginShell();
        }

        protected override void AfterRun()
        {
            Helper.iWrite("SYSTEM STOPPED!! Power off the machine or reboot to continue working");
        }
    }
}
