﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CosmosKernel1.Helpers;

namespace CosmosKernel1.FileSystem
{
    class FileManager
    {
        public static void ReadFile(string fileName)
        {
            File tempFile = FileTable.GetFile(fileName);
            if (tempFile == null)
            {
                Helper.eWriteLine("ERROR: A file with that name does not exist");
                return;
            }

            Helper.iWriteLine("Loading File From Disk");
            byte[] fileRead = new byte[tempFile.numOfBlocks * GLOBALS.FileBlockSize];
            IDE.GetATA().ReadBlock(tempFile.location, tempFile.numOfBlocks, fileRead);

            string fileData = Encoding.ASCII.GetString(fileRead).Trim();

            Helper.iWriteLine("--File: " + tempFile.fileName);
            Helper.iWriteLine(fileData);
        }

        public static void CreateFile(string fileName)
        {
            // Get name of new FIle
            if (FileTable.GetFile(fileName) != null)
            {
                Helper.eWriteLine("ERROR: A file with that name alread exists");
                return;
            }

            // Get File Data to memory
            Helper.pWriteLine("Input File Data for: " + fileName);
            var input = Helper.ReadLine();
            while (string.IsNullOrWhiteSpace(input))
            {
                Helper.eWriteLine("Input is whitepsace or empty!! Please Reenter");
                input = Helper.ReadLine();
            }

            // Compute size
            uint blocks = ((uint)input.Length / GLOBALS.FileBlockSize) + 1;

            // Allocate space for file
            uint locationToWrite = FileTable.AllocateSpace(blocks);

            // Write File to location
            Helper.iWriteLine("Saving File to Disk");

            byte[] inputWrite = new byte[GLOBALS.FileBlockSize * blocks];
            for (int i = 0; i < inputWrite.Length; i++)
            {
                if (i > input.Length)
                {
                    inputWrite[i] = (byte)0;
                }
                else
                    inputWrite[i] = (byte)input[i];
            }
            IDE.GetATA().WriteBlock(locationToWrite, blocks, inputWrite);

            Helper.iWriteLine("File Saved To Disk");

            // Add file to master file list
            FileTable.AddFile(fileName, locationToWrite, blocks);

            Helper.iWriteLine("Saving FileTable To Disk");
            FileTable.SerializeAndSave();
            Helper.iWriteLine("FileTable Saved To Disk");
            
        }

        public static void ListFiles()
        {
            FileTable.ListFiles();
        }

        //public static override 
        //{

        //}

        public static void Format()
        {
            Helper.pWriteLine("IF YOU ARE SURE YOUR WANT TO FORMAT PLEASE ENTER 'FORMAT'");
            Helper.pWriteLine("*NOTE* THIS IS UNREVERSIBLE");
            var input = Helper.ReadLine();
            if (input == "FORMAT")
            {
                FileTable.Format();
            }
            else
                Helper.eWriteLine("FORMAT CANCLED");
        }
    }
}
