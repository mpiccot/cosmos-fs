﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmosKernel1.FileSystem
{
    public class File
    {
        public uint location { get; protected set; }
        public uint numOfBlocks { get; protected set; }
        public string fileName { get; protected set; }

        public File(string name, uint firstBlock, uint numberOfBlocks)
        {
            fileName = name;
            location = firstBlock;
            numOfBlocks = numberOfBlocks;
        }

        public override string ToString()
        {
            return fileName + "," + location + "," + numOfBlocks + ";";
        }
    }
}
