﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cosmos.Hardware.BlockDevice;

namespace CosmosKernel1.FileSystem
{
    public static class IDE
    {
        private static AtaPio xATA = null;

        public static void SetupDevice()
        {
            for (int i = 0; i < BlockDevice.Devices.Count; i++)
            {
                var xDevice = BlockDevice.Devices[i];
                if (xDevice is AtaPio)
                {
                    xATA = (AtaPio)xDevice;
                }
            }
        }

        public static AtaPio GetATA()
        {
            if (xATA == null)
            {
                SetupDevice();
            }
            return xATA;
        }
    }
}
