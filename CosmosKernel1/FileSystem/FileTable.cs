﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using CosmosKernel1.Helpers;

namespace CosmosKernel1.FileSystem
{
    class FileTable
    {

        private static List<File> files = new List<File>();

        public static void SerializeAndSave()
        {
            //Console.WriteLine("Saving FileTable to Disk");
            string fileString = "";
            for (int i = 0; i < files.Count; i++)
            {
                fileString += files[i].ToString();
            }
            uint blocks = ((uint)fileString.Length / GLOBALS.FileBlockSize) + 1;


            string headerInfo = GLOBALS.FileTableLocation + "," + blocks + ",";
            byte[] headerWrite = new byte[GLOBALS.FileBlockSize];
            for (int i = 0; i < GLOBALS.FileBlockSize; i++)
            {
                if (i > headerInfo.Length)
                {
                    headerWrite[i] = (byte)0;
                }
                else
                    headerWrite[i] = (byte)headerInfo[i];
            }
            IDE.GetATA().WriteBlock(GLOBALS.FileTableHeaderLocation, 1, headerWrite);

            //Console.WriteLine("FileTableHeader Saved To Disk");


            var xWrite = new byte[blocks * GLOBALS.FileBlockSize];
            for (int i = 0; i < blocks * GLOBALS.FileBlockSize; i++)
            {
                if (i > fileString.Length)
                {
                    xWrite[i] = (byte)0;
                }
                else
                    xWrite[i] = (byte)fileString[i];
            }
            IDE.GetATA().WriteBlock(GLOBALS.FileTableLocation, blocks, xWrite);
            Helper.iWriteLine("FileTable Saved To Disk");
        }

        public static void DeserializeAndLoad()
        {
            Helper.iWriteLine("Loading FileTable From Disk");
            byte[] headerRead = new byte[GLOBALS.FileBlockSize];
            IDE.GetATA().ReadBlock(287, 1, headerRead);

            string headerInfo = Encoding.ASCII.GetString(headerRead).Trim();
            string[] headerInfoArray = headerInfo.Split(',');

            

            Helper.iWriteLine("FileTableHeader: " + headerInfoArray[0] + "   " + headerInfoArray[1].Trim());

            if (headerInfoArray.Length != 3)
            {
                return;
            }

            uint fileTableLocation = (uint)Int32.Parse(headerInfoArray[0]);
            uint fileTableSize = (uint)Int32.Parse(headerInfoArray[1]);

            Helper.iWriteLine("FileTableHeader Loaded From Disk");


            byte[] xRead = new byte[fileTableSize * GLOBALS.FileBlockSize];
            IDE.GetATA().ReadBlock(fileTableLocation, fileTableSize, xRead);

            string temp2 = Encoding.ASCII.GetString(xRead).Trim();
            string[] filesArray = temp2.Split(';');
            string[] fileParts;

            files.Clear();

            for (int i = 0; i < filesArray.Length-1; i++)
            {
                fileParts = filesArray[i].Split(',');
                AddFile(fileParts[0], (uint)Int32.Parse(fileParts[1]), (uint)Int32.Parse(fileParts[2]));
            }
            Helper.iWriteLine("fileArray: " + filesArray.Length + "     FileTable: " + files.Count);
            Helper.iWriteLine("FileTable Loaded From Disk");
        }

        public static void AddFile(string name, uint location, uint blockCount)
        {
            files.Add(new File(name, location, blockCount));
        }

        public static void ListFiles()
        {
            Helper.iWriteLine("Files: " + files.Count);
            for (int i = 0; i < files.Count; i++)
            {
                string temp = "--" + files[i].fileName + "\t\t" + (files[i].numOfBlocks * GLOBALS.FileBlockSize);
                Helper.iWriteLine(temp + "\t\tString Length: " + temp.Length);
            }
        }

        public static File GetFile(string fileName)
        {
            File file = null;
            for (int i = 0; i < files.Count; i++)
            {
                if (files[i].fileName == fileName)
                {
                    file = files[i];
                    break;
                }
            }

            return file;
        }

        public static uint AllocateSpace(uint blockSize)
        {
            uint location = 700;
            if (files.Count == 0)
                return GLOBALS.FileUnProtectedAreaBegin;

            File file = files[files.Count-1];
            location = file.location + file.numOfBlocks;
           
            return location;
        }

        internal static void Format()
        {
            files = new List<File>();
            SerializeAndSave();
        }
    }
}
