﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sys = Cosmos.System;

namespace CosmosKernel1.Helpers
{
    class Helper
    {
        //Write with a set text and background
        public static void Write(String str, ConsoleColor text, ConsoleColor background)
        {
            Console.ForegroundColor = text;
            Console.BackgroundColor = background;
            Console.Write(str);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.BackgroundColor = ConsoleColor.Black;
        }
        public static void WriteLine(String str, ConsoleColor text, ConsoleColor background)
        {
            Write(str, text, background);
            Console.WriteLine();
        }
        //Write an error
        public static void eWrite(String str)
        {
            Write(str, ConsoleColor.Black, ConsoleColor.Red);
        }
        public static void eWriteLine(String str)
        {
            WriteLine(str, ConsoleColor.Black, ConsoleColor.Red);
        }
        //Write a prompt
        public static void pWrite(String str)
        {
            Write(str, ConsoleColor.White, ConsoleColor.Black);
        }
        public static void pWriteLine(String str)
        {
            WriteLine(str, ConsoleColor.White, ConsoleColor.Black);
        }
        //Write info
        public static void iWrite(String str)
        {
            Write(str, ConsoleColor.DarkGreen, ConsoleColor.Black);
        }
        public static void iWriteLine(String str)
        {
            WriteLine(str, ConsoleColor.DarkGreen, ConsoleColor.Black);
        }
        public static String ReadLine()
        {
            String str;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.Black;
            str = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            return str;
        }
    }
}
