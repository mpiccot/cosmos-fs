﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CosmosKernel1.Helpers;


namespace CosmosKernel1.Helpers
{
    class Shell
    {
        public static void beginShell()
        {
            Helper.pWrite("Input: ");
            var input = Helper.ReadLine();
            if (!string.IsNullOrWhiteSpace(input))
            {
                
                if (input == "reboot")
                {
                    Helper.iWriteLine("Trying to reboot: ");
                    //Restart();
                    //I don't quite understand where this comes from, and why it functions in the kernel and not here
                }
                else if (input == "cf")
                {
                    Helper.pWrite("Name of the new file to create: ");
                    input = Helper.ReadLine();
                    FileSystem.FileManager.CreateFile(input);
                }
                else if (input == "rf")
                {
                    Helper.pWrite("Name of the new file to read: ");
                    input = Helper.ReadLine();
                    FileSystem.FileManager.ReadFile(input);
                }
                else if (input == "dir")
                {
                    FileSystem.FileManager.ListFiles();
                }
                else if (input == "format")
                {
                    FileSystem.FileManager.Format();
                }
                else if (input == "cls")
                {
                    Console.Clear();
                }
                else if (input == "help")
                {
                    Helper.iWriteLine("List of commands:");
                    Helper.iWriteLine("cf - Creates a file");
                    Helper.iWriteLine("rf - Reads a file");
                    Helper.iWriteLine("dir - Lists files");
                    Helper.iWriteLine("format - Resets the file table");
                    Helper.iWriteLine("reboot - Restarts the OS");
                    Helper.iWriteLine("cls - Clears the screen");
                }
                else
                {
                    Helper.eWriteLine("Command Unknown");
                }
            }
        }
    }
}
