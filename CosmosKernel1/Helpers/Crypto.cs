﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace CosmosKernel1.Helpers
{
    class Crypto
    {
        public enum HashType : int
        {
            MD5,
            SHA1,
            SHA256,
            SHA512
        }

        public static string GetHash(string text, HashType hashType)
        {
            HashAlgorithm _algorithm;
            switch (hashType)
            {
                case HashType.MD5:
                    _algorithm = MD5.Create();
                    break;
                case HashType.SHA1:
                    _algorithm = SHA1.Create();
                    break;
                case HashType.SHA256:
                    _algorithm = SHA256.Create();
                    break;
                case HashType.SHA512:
                    _algorithm = SHA512.Create();
                    break;
                default:
                    throw new ArgumentException("Invalid hash type", "hashType");
            }
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            byte[] hash = _algorithm.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public static bool CheckHash(string original, string hashString, HashType hashType)
        {
            string originalHash = GetHash(original, hashType);
            return (originalHash == hashString);
        }
    }
}
