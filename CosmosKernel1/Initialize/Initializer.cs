﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cosmos.Core;
using Sys = Cosmos.System;
using CosmosKernel1.Helpers;

namespace CosmosKernel1
{
    class Initializer
    {
        public static GLOBALS.ReturnCode Initialize()
        {
            GLOBALS.ReturnCode initReturnCode = CheckSystems();

            if (initReturnCode != GLOBALS.ReturnCode.Success)
            {
                Helper.eWrite("Initializer Error: ");

                switch (initReturnCode)
                {
                    case GLOBALS.ReturnCode.Error: Helper.eWrite("General Error");
                        break;
                    case GLOBALS.ReturnCode.ErrorHardDrive: Helper.eWrite("Hard Drive Error");
                        break;
                    case GLOBALS.ReturnCode.ErrorFileSystem: Helper.eWrite("File System Error");
                        break;
                    case GLOBALS.ReturnCode.ErrorMemory: Helper.eWrite("Memory Error");
                        break;
                    case GLOBALS.ReturnCode.ErrorVersion: Helper.eWrite("Version Error");
                        break;
                    default: Helper.eWrite("Unknown Error");
                        break;
                }
                Console.WriteLine();                
            }
            return initReturnCode;
        }

        private static GLOBALS.ReturnCode CheckSystems()
        {
            GLOBALS.ReturnCode currentReturnCode = GLOBALS.ReturnCode.Error;

            #region Version
            currentReturnCode = Version();
            if (currentReturnCode != GLOBALS.ReturnCode.Success)
                return currentReturnCode;
            #endregion

            #region Memory
            currentReturnCode = MemoryCheck();
            if (currentReturnCode != GLOBALS.ReturnCode.Success)
                return currentReturnCode;
            #endregion

            #region HardDrive
            currentReturnCode = HardDriveCheck();
            if (currentReturnCode != GLOBALS.ReturnCode.Success)
                return currentReturnCode;
            #endregion

            #region FileSystem
            currentReturnCode = FileSystemCheck();
            if (currentReturnCode != GLOBALS.ReturnCode.Success)
                return currentReturnCode;
            #endregion


            return currentReturnCode;
        }

        private static GLOBALS.ReturnCode HardDriveCheck()
        {
            Cosmos.Hardware.BlockDevice.AtaPio hd = FileSystem.IDE.GetATA();

            if (hd == null)
                return GLOBALS.ReturnCode.ErrorHardDrive;

            Helper.iWriteLine("--------------------------");
            Helper.iWriteLine("Type: " + (hd.DriveType == Cosmos.Hardware.BlockDevice.AtaPio.SpecLevel.ATA ? "ATA" : "ATAPI"));
            Helper.iWriteLine("Serial No: " + hd.SerialNo);
            Helper.iWriteLine("Firmware Rev: " + hd.FirmwareRev);
            Helper.iWriteLine("Model No: " + hd.ModelNo);
            Helper.iWriteLine("Block Size: " + hd.BlockSize + " bytes");
            Helper.iWriteLine("Size: " + hd.BlockCount * hd.BlockSize / 1024 / 1024 + " MB");

            return GLOBALS.ReturnCode.Success;
        }

        private static GLOBALS.ReturnCode Version()
        {
            Helper.iWriteLine(GLOBALS.OSName + " v" + GLOBALS.OSVersionNum + " Booting...");

            return GLOBALS.ReturnCode.Success;
        }
        private static GLOBALS.ReturnCode MemoryCheck()
        {
            try
            {
                Helper.iWriteLine("Checking Memory...");
                uint mem = CPU.GetAmountOfRAM();
                Helper.iWrite("Memory: " + (mem + 2) + " MB ");
                Helper.iWriteLine("OK");
            }
            catch (Exception)
            {
                return GLOBALS.ReturnCode.ErrorMemory;
            }
            

            return GLOBALS.ReturnCode.Success;
        }
        private static GLOBALS.ReturnCode FileSystemCheck()
        {
            return GLOBALS.ReturnCode.Success;
        }
        private static GLOBALS.ReturnCode FileTableCheck()
        {
            FileSystem.FileTable.DeserializeAndLoad();
            return GLOBALS.ReturnCode.Success;
        }

        
    }
}
