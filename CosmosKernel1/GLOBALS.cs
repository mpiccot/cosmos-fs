﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmosKernel1
{
    class GLOBALS
    {
        public static readonly string       OSName = "UNCCOS";
        public static readonly string OSVersionNum = "0.1";

        public static readonly string OSNameASCIILogo = @"  _    _ _   _  _____ _____ ____   _____ " + "\n" +
                                                        @" | |  | | \ | |/ ____/ ____/ __ \ / ____|" + "\n" +
                                                        @" | |  | |  \| | |   | |   | |  | | (___  " + "\n" +
                                                        @" | |  | | . ` | |   | |   | |  | |\___ \ " + "\n" +
                                                        @" | |__| | |\  | |___| |___| |__| |____) |" + "\n" +
                                                        @"  \____/|_| \_|\_____\_____\____/______/ " + "\n" +
                                                        "\n";

        // FILESYSTEM INFO
        public static readonly uint FileTableHeaderLocation = 287;
        public static readonly uint FileTableLocation = FileTableHeaderLocation + 1;
        public static readonly uint FileBlockSize = 512;
        public static readonly uint FileUnProtectedAreaBegin = 500;

        public enum ReturnCode
        {
            Success,
            Error,
            ErrorHardDrive,
            ErrorFileSystem,
            ErrorMemory,
            ErrorVersion
        }
    }
}
